/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ufps.modelo.Persona;

/**
 *
 * @author Harold
 */
public class ListaSTest_Prueba3 {
    
    public ListaSTest_Prueba3() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of eliminar method, of class ListaS.
     */
    @Test
    public void testEliminar1() {
        System.out.println("Eliminar 1");
        
        int i = 2;
        ListaS<Integer> instance = new ListaS();
        instance.insertarAlInicio(8);
        instance.insertarAlInicio(806);
        instance.insertarAlInicio(408);
        instance.insertarAlInicio(115);
        Object esperado = instance.get(i);
        Object resultado = instance.eliminar(i);
        
        assertEquals(esperado, resultado);        
    }
    
    @Test
    public void testEliminar2() {
        System.out.println("Eliminar 2");
        
        int i = 4;
        ListaS<String> resultado = new ListaS();
        resultado.insertarAlInicio("Maria");
        resultado.insertarAlInicio("Josue");
        resultado.insertarAlInicio("Mimi");
        resultado.insertarAlInicio("Reynaldo");
        resultado.insertarAlInicio("Yaneth");
        resultado.insertarAlInicio("Zuleika");
        
        ListaS<String> esperado = new ListaS();
        esperado.insertarAlInicio("Maria");
        esperado.insertarAlInicio("Mimi");
        esperado.insertarAlInicio("Reynaldo");
        esperado.insertarAlInicio("Yaneth");
        esperado.insertarAlInicio("Zuleika");
                
        resultado.eliminar(i);
        assertEquals(esperado,resultado);
    }

    /**
     * Test of aVector method, of class ListaS.
     */
    @Test
    public void testAVector1() {
        System.out.println("aVector 1");
        ListaS<Integer> instance = new ListaS();
        instance.insertarAlInicio(20);
        instance.insertarAlInicio(890);
        instance.insertarAlInicio(90);
        instance.insertarAlInicio(165);
        instance.insertarAlInicio(8);
        Object[] esperado = {8,165,90,890,20};
        Object[] resultado = instance.aVector();        
        
        assertArrayEquals(esperado, resultado);
    }
    
    /**
     * Test of aVector method, of class ListaS.
     */
    @Test
    public void testAVector2() {
        System.out.println("aVector 2");
        ListaS<String> instance = new ListaS();
        instance.insertarAlFinal("Pilar");
        instance.insertarAlFinal("Antonio");
        instance.insertarAlFinal("Pancracio");
        instance.insertarAlFinal("Harold");
        instance.insertarAlFinal("Fabiola");
        instance.insertarAlFinal("Daniela");
        Object[] esperado = {"Pilar","Antonio","Pancracio","Harold","Fabiola","Daniela"};
        Object[] resultado = instance.aVector();        
        
        assertArrayEquals(esperado, resultado);
    }   

    /**
     * Test of insertionSortNodo method, of class ListaS.
     */
    @Test
    public void testInsertionSortNodo1() {
        System.out.println("insertionSortNodo 1");
        ListaS<Persona> resultado = new ListaS();
        resultado.insertarAlInicio(new Persona("Harold",116));
        resultado.insertarAlInicio(new Persona("Maria",999));
        resultado.insertarAlInicio(new Persona("Johana",80));
        resultado.insertarAlInicio(new Persona("Reynaldo",198));
        resultado.insertarAlInicio(new Persona("Yaneth",119));
        
                
        ListaS<Persona> esperado = new ListaS();        
        esperado.insertarAlFinal(new Persona("Johana",80));
        esperado.insertarAlFinal(new Persona("Harold",116));
        esperado.insertarAlFinal(new Persona("Yaneth",119));
        esperado.insertarAlFinal(new Persona("Reynaldo",198));
        esperado.insertarAlFinal(new Persona("Maria",999));

        
        resultado.insertionSortNodo();    
        
        assertEquals(esperado,resultado);
        
    }
    
    /**
     * Test of insertionSortNodo method, of class ListaS.
     */
    @Test
    public void testInsertionSortNodo2() {
        System.out.println("insertionSortNodo 2");
        ListaS<Integer> resultado = new ListaS();
        resultado.insertarAlInicio(908);
        resultado.insertarAlInicio(185);
        resultado.insertarAlInicio(90);
        resultado.insertarAlInicio(6);
        resultado.insertarAlInicio(180);
                
        ListaS<Integer> esperado = new ListaS();
        esperado.insertarAlFinal(6);
        esperado.insertarAlFinal(90);
        esperado.insertarAlFinal(180);
        esperado.insertarAlFinal(185);
        esperado.insertarAlFinal(908);
        
        resultado.insertionSortNodo();
        
        assertEquals(esperado,resultado);
    }

    /**
     * Test of equals method, of class ListaS.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals 1");
        ListaS<Persona> instance = new ListaS();
        instance.insertarAlInicio(new Persona("Harold",1004));
        instance.insertarAlInicio(new Persona("Reynaldo",134));
        instance.insertarAlInicio(new Persona("Yaneth",2783));
        instance.insertarAlInicio(new Persona("Zuleika",1090));
        instance.insertarAlInicio(new Persona("Maria",1005));
        ListaS<Persona> instance2 = new ListaS();
        instance2.insertarAlInicio(new Persona("Harold",1004));
        instance2.insertarAlInicio(new Persona("Reynaldo",134));
        instance2.insertarAlInicio(new Persona("Yaneth",2783));
        instance2.insertarAlInicio(new Persona("Zuleika",1090));
        instance2.insertarAlInicio(new Persona("Maria",1005));
        
        boolean equal=instance2.equals(instance);
        assertTrue(equal);
    }
    
    /**
     * Test of equals method, of class ListaS.
     */
    @Test
    public void testEquals2() {
        System.out.println("equals 2");
        ListaS<Integer> instance = new ListaS();
        instance.insertarAlFinal(5);
        instance.insertarAlFinal(50);
        instance.insertarAlFinal(80);
        instance.insertarAlFinal(9);
        instance.insertarAlFinal(99);
        ListaS<Integer> instance2 = new ListaS();
        instance2.insertarAlFinal(5);
        instance2.insertarAlFinal(50);
        instance2.insertarAlFinal(80);
        instance2.insertarAlFinal(9);
        instance2.insertarAlFinal(99);
        
        boolean equal=instance2.equals(instance);
        assertTrue(equal);
    }
    
}
