/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author Harold
 */
public class TestEliminarIndice {

    public static void main(String[] args) {
        ListaCD l1 = new ListaCD();

        l1.insertarAlFinal(2);
        l1.insertarAlFinal(3);
        l1.insertarAlFinal(4);
        l1.insertarAlFinal(5);
        l1.insertarAlFinal(6);
        l1.insertarAlFinal(7);
        try{
        l1.borrarIntervalo(0, 2);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        System.out.println(l1.toString());
        System.out.println(l1.getTamanio());
    }

}
