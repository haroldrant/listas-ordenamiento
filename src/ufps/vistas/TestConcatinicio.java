/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author Harold
 */
public class TestConcatinicio {
    public static void main (String [] args){
        ListaCD l1 = new ListaCD();
        ListaCD l2 = new ListaCD();
        
        l1.insertarAlFinal(1);
        l1.insertarAlFinal(2);
        l2.insertarAlFinal(3);
        l2.insertarAlFinal(4);
        l1.concatinicio(l2);
        System.out.println(l1.toString());
    }
}
